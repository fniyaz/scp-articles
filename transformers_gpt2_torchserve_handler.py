from abc import ABC
import json
import logging
import os

import torch
from transformers import AutoModelForCausalLM, AutoTokenizer, AutoConfig

from ts.torch_handler.base_handler import BaseHandler

logger = logging.getLogger(__name__)


class TransformersGeneratorHandler(BaseHandler, ABC):
    """
    Transformers text classifier handler class. This handler takes a text (string) and
    as input and returns the classification text based on the serialized transformers checkpoint.
    """
    def __init__(self):
        super(TransformersGeneratorHandler, self).__init__()
        self.initialized = False

    def initialize(self, ctx):
        self.manifest = ctx.manifest

        properties = ctx.system_properties
        model_dir = properties.get("model_dir")
        self.device = torch.device("cuda:" + str(properties.get("gpu_id")) if torch.cuda.is_available() else "cpu")

        # Read model serialize/pt file
        config = AutoConfig.from_pretrained(model_dir)
        self.model = AutoModelForCausalLM.from_pretrained(model_dir, config=config)
        self.tokenizer = AutoTokenizer.from_pretrained("gpt2")
        self.tokenizer.pad_token = self.tokenizer.eos_token

        self.model.to(self.device)
        self.model.eval()

        logger.debug('Transformer model from path {0} loaded successfully'.format(model_dir))

        # Read the mapping file, index to object name
        mapping_file_path = os.path.join(model_dir, "index_to_name.json")

        if os.path.isfile(mapping_file_path):
            with open(mapping_file_path) as f:
                self.mapping = json.load(f)
        else:
            logger.warning('Missing the index_to_name.json file. Inference output will not include class name.')

        self.initialized = True

    def preprocess(self, data):
        """ Very basic preprocessing code - only tokenizes. 
            Extend with your own preprocessing steps as needed.
        """
        text = data[0].get("data")
        if text is None:
            text = data[0].get("body")
        if type(text) == str:
            sentence = text
        else:
            sentence = text.decode('utf-8')
        logger.info("Received text: '%s'", sentence)

        inputs = self.tokenizer.encode(
            self.tokenizer.bos_token + sentence,
            return_tensors="pt"
        )
        return inputs

    def inference(self, inputs):
        """
        Predict the class of a text using a trained transformer model.
        """
        # NOTE: This makes the assumption that your model expects text to be tokenized  
        # with "input_ids" and "token_type_ids" - which is true for some popular transformer models, e.g. bert.
        # If your transformer model expects different tokenization, adapt this code to suit 
        # its expected input format.

        with torch.cuda.amp.autocast():
            sample_output = self.model.generate(
                inputs.to(self.device),
                do_sample=True, 
                max_length=512, 
                top_k=50, 
                top_p=0.95, 
                num_return_sequences=1
            )
        logger.info(f"sample {str(sample_output)}")
        prediction = self.tokenizer.decode(sample_output[0], skip_special_tokens=True)
        logger.info("Model predicted: '%s'", prediction)

        return [prediction]

    def postprocess(self, inference_output):
        # TODO: Add any needed post-processing of the model predictions here
        return inference_output


_service = TransformersGeneratorHandler()


def handle(data, context):
    try:
        if not _service.initialized:
            _service.initialize(context)

        if data is None:
            return None

        data = _service.preprocess(data)
        data = _service.inference(data)
        data = _service.postprocess(data)

        return data
    except Exception as e:
        raise e
