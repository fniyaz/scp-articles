pip install torchserve torch-model-archiver
torch-model-archiver --model-name "gpt2" --version 1.0 --serialized-file ./model/pytorch_model.bin --extra-files "./model/config.json" --handler "./transformers_gpt2_torchserve_handler.py"
mkdir model_store
mv gpt2.mar model_store
touch config.properties
torchserve --stop
torchserve --start --model-store model_store --models gpt2=gpt2.mar --ts-config config.properties