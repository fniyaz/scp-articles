# Install Hugging Face transformers
!pip install -q git+https://github.com/huggingface/transformers/
!git clone https://gitlab.com/fniyaz/scp-articles.git
!unzip scp-articles/model.zip

import torch
from transformers import AutoConfig, AutoTokenizer, AutoModelForCausalLM

MODEL_TYPE = "gpt2"

tokenizer = AutoTokenizer.from_pretrained(MODEL_TYPE)
tokenizer.pad_token = tokenizer.eos_token

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
config = AutoConfig.from_pretrained('./model/config.json')
model = AutoModelForCausalLM.from_pretrained('./model/pytorch_model.bin', config=config).to(device)

# encode context the generation is conditioned on
input_ids = tokenizer.encode(tokenizer.bos_token + "Item #:", return_tensors='pt').to(device)

with torch.cuda.amp.autocast():
  sample_outputs = model.generate(
      input_ids,
      do_sample=True, 
      max_length=2048, 
      top_k=50, 
      top_p=0.95, 
      num_return_sequences=1
  )

print("Output:\n" + 100 * '-')
for i, sample_output in enumerate(sample_outputs):
  print("{}: \n{}".format(i, tokenizer.decode(sample_output, skip_special_tokens=True)))