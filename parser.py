import os
import requests
from bs4 import BeautifulSoup
from tqdm.contrib.concurrent import thread_map
from time import sleep


# For retries on error when downloading
SLEEP_TIME = 0.05 # in seconds
MAX_TRIES = 10

SCP_COUNT = 6000
THREAD_POOL_SIZE = 64


def get_page(url):
    try:
        r = requests.get(url)
    except requests.exceptions.RequestException:
        return None

    if r.status_code == 200:
        return r.text
    else:
        return None


def generate_url(i):
    if i < 1000:
        i_str = ("0"*3 + str(i)) [-3:]  # I am too lazy to print into format 001 - 999
        scp_text = f"scp-{i_str}"
    else:
        scp_text = f"scp-{i}"
    return f"http://www.scpwiki.com/{scp_text}", scp_text


def parse(html):
    # get the content
    bs = BeautifulSoup(html, features="html.parser")
    cont = bs.find(attrs={"id": "page-content"})

    # filter useless material
    _class_to_filter = ["footer-wikiwalk-nav", "licensebox", "page-rate-widget-box"]
    for class_ in _class_to_filter:
        for tag in cont.find_all("div", attrs={"class": class_}):
            tag.extract()

    # a draft for saving the structure of the file as well
    # _sections = ["Special Containment Procedures", "Description", "Item", "Object Class", "Additional Notes"]
    #
    # def is_section(string: str):
    #     return any([s in string for s in _sections])
    #
    # for tag in cont.find_all("p"):
    #     for strong in tag.find_all("strong"):
    #         if strong.string is not None:
    #             if is_section(strong.string):
    #                 removed = strong.string.extract()
    #                 strong.insert_before(f"[B]{removed}[/B]")

    # extract text
    text = "\n".join([tag.get_text() for tag in cont.find_all("p")])

    return text


def parse_scp(scp_number: int):
    url, scpN = generate_url(scp_number)

    page = None
    for i in range(MAX_TRIES):
        page = get_page(url)
        if page is not None:
            break
        sleep(SLEEP_TIME)
    if page is None:
        print(f"Warning: error downloading page number {scpN}.")
        return
    text = parse(page)
    with open(f"parsed/{scpN}", "w") as w:
        w.write(text)


def main():
    try:
        os.mkdir("parsed")
    except FileExistsError:
        pass

    thread_map(parse_scp, range(SCP_COUNT), max_workers=THREAD_POOL_SIZE)


if __name__ == '__main__':
    main()
