module Utils exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


defaultPaddings
  = [ style "padding-left"   "150px"
    , style "padding-right"  "150px"
    , style "padding-top"    "100px"
    , style "padding-bottom" "100px"
    ]


errorBox msg callback = 
  div (  [ align "center" ]
      ++ [ style "background-color" "#ef7975", style "border" "2px solid #951c30" ]
      ++ [ style "color" "#59111d" ]
      )
    [ div [ onClick callback, align "right", style "margin-top" "5px", style "margin-right" "5px" ] [ text "X" ]
    , h3 [ style "margin-top" "-5px" ] [ text msg ]
    ]

breakLines : List (Html msg) -> String -> List (Html msg)
breakLines breaks = List.concat << List.map (\l -> [text l] ++ breaks) << String.split "\n"
