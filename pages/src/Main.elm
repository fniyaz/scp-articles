module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http

import Utils exposing (..)


server_address : String
server_address = "http://18.184.246.172:5000"



-- MAIN

main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }


type Model
  = RequestPage RequestModel


type Msg 
  = RequestMsg RequestMsg


init : () -> (Model, Cmd Msg)
init _ = (RequestPage emptyRequset, Cmd.none)


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case model of
    (RequestPage page) -> case msg of 
        (RequestMsg msg_) -> updateRequest msg_ page



view : Model -> Html Msg
view model = 
  case model of (RequestPage page) -> viewRequest page


subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.none



-- Request

type alias RequestModel =
  { msg: String
  , loading: Bool
  , error: String
  , article: String
  }

emptyRequset : RequestModel
emptyRequset = { msg = ""
               , loading = False
               , error = ""
               , article = ""
               }

type RequestMsg
  = UpdateText String
  | SendRequest
  | GotText (Result Http.Error String)
  | ClearError


updateRequest : RequestMsg -> RequestModel -> (Model, Cmd Msg)
updateRequest msg model =
  case msg of (UpdateText text) -> (RequestPage { model | msg = text }, Cmd.none)
              SendRequest -> (RequestPage { model | loading = True }, Http.post
                                      { url = server_address
                                      , body = Http.stringBody "text/plain" model.msg
                                      , expect = Http.expectString (RequestMsg << GotText)
                                      })
              (GotText result) -> case result of
                Ok text -> (RequestPage { model | loading = False, error = "", article = text }, Cmd.none)
                Err err -> (RequestPage { model | loading = False, error = toStr err }, Cmd.none)
              ClearError -> (RequestPage { model | error = "" }, Cmd.none)


toStr err = case err of 
    Http.BadUrl str     -> "BadUrl " ++ str
    Http.Timeout        -> "Timeout"
    Http.NetworkError   -> "NetworkError"
    Http.BadStatus i    -> "BadStatus " ++ (String.fromInt i)
    Http.BadBody str    -> "BadBadBody " ++ str


viewRequest : RequestModel -> Html Msg
viewRequest page =
  div [ style "height" "100vh", style "background-color" "#101010" ] [
    div (defaultPaddings ++ [ align "left", style "background-color" "#101010" ])
      ( 
        [ div [ style "background-color" "#383838"
              , style "padding" "50px"
              , style "border-radius" "25px"
              ] 
            (
              [ textarea ( [ rows 7, style "width" "100%", style "resize" "none"]
                         ++ [ style "border" "none", style "outline-color" "white", style "padding" "15px", style "margin" "15px -15px" ]
                         ++ [ style "background-color" "#1e1e1e", style "color" "white" ]
                         ++ [placeholder "request", value page.msg, onInput (RequestMsg << UpdateText)]
                         ) []
              , br [] []
              , button ( [ disabled page.loading, onClick (RequestMsg SendRequest) ]
                       ++ [ style "background-color" "#eeeeee", style "border" "none", style "padding" "10px", style "font-size" "16px" ]
                       )[ text "Generate" ]
              , br [] []
              , div [ style "background-color" "white", style "height" "1px", style "width" "100%", style "margin" "25px 0px" ] []
              ]
              ++ (if String.isEmpty page.error then [] else [ errorBox page.error (RequestMsg ClearError) ])
              ++ (if page.loading then [viewLoading] else [])
              ++ (if String.isEmpty page.article then [] else [ viewArticle page.article ])
            )
        ]
      )
    ]


viewArticle : String -> Html Msg
viewArticle page = 
    div [ style "color" "white", align "left" ]
      (breakLines [br [] [], br [] []] page)

viewLoading : Html Msg
viewLoading = h1 [ style "color" "white" ] [ text "Generating text" ]
